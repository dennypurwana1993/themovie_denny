//
//  MovieDetailView.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//


import UIKit
/*STEP 1 ==>
 VIEW ==> Bluprint suatu modul.
 mendeklarasikan fungsi2 yang ada pada modul movie detail,
 yang nantinya akan di akses oleh moviedetailviewcontroller
 */
protocol MovieDetailViewInputs: AnyObject {
    func configure(data: MovieDetail)
    func showYoutubeVideo(datas: [MovieVideo]?)
    func showReviews(review: ReviewsEntities)
    func reloadCollectionView(collectionViewDataSource: ReviewsCollectionViewDataSource)
    func indicatorView(animate: Bool)
}

protocol MovieDetailViewOutputs: AnyObject {
    func viewDidLoad()
    
}
