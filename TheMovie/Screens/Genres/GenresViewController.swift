//
//  GenresViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

final class GenresViewController: UIViewController {

    internal var presenter: GenresViewOutputs?
    internal var collectionViewDataSource: CollectionViewDataSource?

    @IBOutlet weak var genresCollectionView: UICollectionView! {
        didSet {
            genresCollectionView.delegate = self
            genresCollectionView.dataSource = self
            genresCollectionView.register(UINib.init(nibName: "GenreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "genreCell")
        }
    }
   
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

}

extension GenresViewController: GenresViewInputs {

    func configure(entities: GenreEntities) {
        navigationItem.title = "Genre"
    }

    func reloadCollectionView(collectionViewDataSource: GenresCollectionViewDataSource) {
        self.collectionViewDataSource = collectionViewDataSource
        DispatchQueue.main.async { [weak self] in
            self?.genresCollectionView.reloadData()
        }
    }

    func indicatorView(animate: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.genresCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: animate ? 50 : 0, right: 0)
            _ = animate ? self?.indicatorView.startAnimating() : self?.indicatorView.stopAnimating()
        }
    }
}

extension GenresViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewDataSource?.numberOfItems ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionViewDataSource?.itemCell(collectionView: collectionView, indexPath: indexPath) ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        return collectionViewDataSource?.didSelect(collectionView: collectionView, indexPath: indexPath) ?? ()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
            let width = collectionView.bounds.width / 2
            return CGSize(width: width, height: 42 )
           
       }
   
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
    }
}

extension GenresViewController: Viewable {}
