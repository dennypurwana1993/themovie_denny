//
//  MovieTop250View.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/24/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

protocol MovieTop250ViewInputs: AnyObject {
   
    func configure(entities: MovieTop250Entities)
    func reloadCollectionView(collectionViewDataSource: MovieTop250CollectionViewDataSource)
    func indicatorView(animate: Bool)
    
}

protocol MovieTop250ViewOutputs: AnyObject {
    func viewDidLoad()
    func onReachBottom()
}
