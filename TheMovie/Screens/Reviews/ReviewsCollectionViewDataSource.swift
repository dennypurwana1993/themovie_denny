//
//  ReviewsCollectionViewDataSource.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

protocol ReviewsCollectionViewDataSourceOutputs: AnyObject {
    func didSelect(_ review: Review)
}

final class ReviewsCollectionViewDataSource: CollectionViewDataSource {
    
    func didSelect(key: String, collectionView: UICollectionView, indexPath: IndexPath) {
        
    }
    
    func itemCell(key: String, collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
    
    func getNumberOfItems(key: String) -> Int {
        return 0
        
    }
    
    private weak var entities: ReviewsEntities!
    private weak var presenter: ReviewsCollectionViewDataSourceOutputs?
   
    var selectedIndex = Int ()
    
    init(entities: ReviewsEntities, presenter: ReviewsCollectionViewDataSourceOutputs) {
        self.entities = entities
        self.presenter = presenter
    }
    
    var numberOfItems: Int {
        return entities.reviews.count ?? 0
    }
    
    func itemCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let review = entities.reviews[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reviewsCell", for: indexPath) as! ReviewsCollectionViewCell
        cell.configure(with: review)
        return cell
    }
    
    func didSelect(collectionView: UICollectionView, indexPath: IndexPath) {
        let selectedGenre = entities.reviews[indexPath.row]
        presenter?.didSelect(selectedGenre)
        selectedIndex = indexPath.row
        collectionView.reloadData()
    }
    
}
