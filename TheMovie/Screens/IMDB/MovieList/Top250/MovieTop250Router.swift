//
//  MovieTop250Router.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/24/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

struct MovieTop250RouterInput {

     func view() -> MovieTop250ViewController {
        let view = MovieTop250ViewController()
        let interactor = MovieTop250Interactor()
        let dependencies = MovieTop250PresenterDependencies(interactor: interactor, router: MovieTop250Output(view))
        let presenter = MovieTop250Presenter(entities: MovieTop250Entities(), view: view, dependencies: dependencies)
        view.presenter = presenter
        view.topRatedCollectionViewDataSource = MovieTop250CollectionViewDataSource(entities: presenter.entities, presenter: presenter)
        interactor.presenter = presenter
        return view
    }

    func push(from: Viewable) {
        print("PUSH VIEW CONTROLLER")
        let view = self.view()
        from.push(view, animated: true)
    }

    func present(from: Viewable) {
        let nav = UINavigationController(rootViewController: view())
        from.present(nav, animated: true)
    }
}

final class MovieTop250Output: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }
    
    func showMovieDetail(movieDetailEntities: MovieDetailImdEntities) {
        MovieDetailImdbRouterInput().present(from: view, entity: movieDetailEntities)
    }
}
