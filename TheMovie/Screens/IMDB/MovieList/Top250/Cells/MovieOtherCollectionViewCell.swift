//
//  MovieOtherCollectionViewCell.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/24/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class MovieOtherCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLblRight: UILabel!
    @IBOutlet weak var posterImageViewRight: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var posterImageViewLeft: UIImageView!
    @IBOutlet weak var cardView: CardView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    public func configure(with movie: MovieTop250) {
   
        posterImageViewLeft.kf.setImage(with: URL(string: movie.image ?? ""), placeholder: UIImage(named: "clapperboard"))
        titleLbl.text = movie.title
        let rankInt = Int(movie.rank ?? "0") ?? 0
        if(rankInt % 2 == 0){
            showView(key: 0)
            posterImageViewRight.kf.setImage(with: URL(string: movie.image ?? ""), placeholder: UIImage(named: "clapperboard"))
            titleLblRight.text = movie.title

        } else {
            showView(key: 1)
            posterImageViewLeft.kf.setImage(with: URL(string: movie.image ?? ""), placeholder: UIImage(named: "clapperboard"))
            titleLbl.text = movie.title
        }

    }
    
    func showView(key : Int){
        if(key == 0){
            posterImageViewRight.makeRoundCorners(byRadius: 5)
            posterImageViewRight.isHidden = false
            titleLblRight.isHidden = false
            posterImageViewLeft.isHidden = true
            titleLbl.isHidden = true
        } else {
            posterImageViewLeft.makeRoundCorners(byRadius: 5)
            posterImageViewRight.isHidden = true
            titleLblRight.isHidden = true
            posterImageViewLeft.isHidden = false
            titleLbl.isHidden = false
        }
    }
    

    
}
