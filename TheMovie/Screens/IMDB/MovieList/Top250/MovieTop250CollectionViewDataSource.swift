//
//  MovieTop250CollectionViewDataSource.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/24/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

protocol MovieTop250CollectionViewDataSourceOutputs: AnyObject {
    func didSelect(_ movie: MovieTop250)
}

final class MovieTop250CollectionViewDataSource: CollectionViewDataSource {
   

    private weak var entities: MovieTop250Entities!
    private weak var presenter: MovieTop250CollectionViewDataSourceOutputs?
   
    init(entities: MovieTop250Entities, presenter: MovieTop250CollectionViewDataSourceOutputs) {
        self.entities = entities
        self.presenter = presenter
    }
    
    var numberOfItems: Int {
        return 0
    }
    
    func getNumberOfItems(key: String) -> Int {
        let countMovieArray: [MovieTop250] = (self.entities.movies[key] ?? []) ?? []
        return countMovieArray.count
    }
    
    func itemCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
    
    func itemCell(key :String, collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        if(key == "topRated"){
            let topRatedmovies: [MovieTop250] = (entities.movies["topRated"] ?? []) ?? []
            let movie = topRatedmovies[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topRatedCell", for: indexPath) as! TopRatedCollectionViewCell
            cell.configure(with: movie)
            return cell
            
        } else {
            
            let othersMovies: [MovieTop250] = (entities.movies["otherMovie"] ?? []) ?? []
            let otherMovie = othersMovies[indexPath.row]
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "otherCell", for: indexPath) as! MovieOtherCollectionViewCell
            cell.configure(with: otherMovie)
            return cell
            
        }
    }
    
    func didSelect(key: String, collectionView: UICollectionView, indexPath: IndexPath) {
        let movies: [MovieTop250] = (entities.movies[key] ?? []) ?? []
        let selectedMovie = movies[indexPath.row]
        presenter?.didSelect(selectedMovie)
        
    }
    
    func didSelect(collectionView: UICollectionView, indexPath: IndexPath) {
       
//        let selectedMovie = entities.topRatedMovies[indexPath.row]
//        presenter?.didSelect(selectedMovie)
        
    }
    
}
