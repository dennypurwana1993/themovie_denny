//
//  MoviesView.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

protocol MoviesViewInputs: AnyObject {
   
    func configure(entities: MovieEntities)
    func reloadCollectionView(collectionViewDataSource: MoviesCollectionViewDataSource)
    func indicatorView(animate: Bool)
    
}

protocol MoviesViewOutputs: AnyObject {
    func viewDidLoad()
    func onReachBottom()
}
