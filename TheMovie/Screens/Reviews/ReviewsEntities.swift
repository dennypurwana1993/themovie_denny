//
//  ReviewsEntities.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

final class ReviewsEntities {
    var reviews: [Review] = []
    
    class ApiState {
        var pageCount = 1
        var movieId = 1
        var movieTitle = ""
        var isFetching = false
    }
    
    var apiState = ApiState()
}
