//
//  DashboardPresenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

typealias DashboardPresenterDependencies = (
    interactor: MoviesInteractor,
    router: DashboardRouterOutput
)

typealias DashboardPresenterDependencies2 = (
    interactor: GenresInteractor,
    router: DashboardRouterOutput
)

final class DashboardPresenter: Presenterable {
    
    internal var movieEntities: MovieEntities!
    internal var genreEntities: GenreEntities!
    private weak var view: DashboardViewInputs!
    let dependencies: DashboardPresenterDependencies
    let dependencies2: DashboardPresenterDependencies2
    
    init(movieEntities: MovieEntities, genreEntities: GenreEntities,
         view: DashboardViewInputs,
         dependencies: DashboardPresenterDependencies, dependencies2: DashboardPresenterDependencies2)
    {
        self.view = view
        self.movieEntities = movieEntities
        self.genreEntities = genreEntities
        self.dependencies = dependencies
        self.dependencies2 = dependencies2
    }
    
}

extension DashboardPresenter: DashboardViewOutputs {
    
    func viewDidLoad() {
        view.configure(genreEntities: genreEntities, movieEntities: movieEntities)
        genreEntities.genreApiState.isFetching = true
        movieEntities.apiState.isFetching = true
        dependencies2.interactor.loadGenres()
        dependencies.interactor.loadDiscoverMovieByGenre(genre: movieEntities.apiState.genreId, page: movieEntities.apiState.pageCount)
        
    }
    
    func showAllMoviesByGenre(){
        print("SHOW ALL MOVIE PASSING PARAM \(movieEntities.apiState.genreId) ==> \(movieEntities.apiState.genreName)")
        dependencies.router.showAllMovieByGenre(movieEntities: movieEntities)
    }
    
    func onReachBottom() {
        print("ONREACH BOTTOM")
        print(movieEntities.apiState.isFetching)
        print(movieEntities.apiState.genreId)
        print(movieEntities.apiState.pageCount)
//        guard !movieEntities.apiState.isFetching else { return }
//        movieEntities.apiState.isFetching = true
        dependencies.interactor.loadDiscoverMovieByGenre(genre: movieEntities.apiState.genreId, page: movieEntities.apiState.pageCount)
//        view.indicatorView(animate: true)
    }
    
}

extension DashboardPresenter: MoviesInteractorOutputs, GenresInteractorOutputs {
    
    func onSuccess(datas: Genres) {
        genreEntities.genreApiState.isFetching = false
        genreEntities.genreApiState.pageCount += 1
        genreEntities.genres += datas.genres ?? []
        view.reloadGenreCollectionView(gCollectionViewDataSource: GenresCollectionViewDataSource(entities: genreEntities, presenter: self))
    }
    
    func onSuccess(datas: Movies) {
        movieEntities.apiState.isFetching = false
        movieEntities.apiState.pageCount += 1
        // entities.gitHubRepositories += res.items
        movieEntities.movies += datas.results ?? []
        view.reloadMovieCollectionView(mCollectionViewDataSource:  MoviesCollectionViewDataSource(entities: movieEntities, presenter: self))
    }
    
    func onError(error: Error) {
        view.indicatorView(animate: false)
    }
    
}

extension DashboardPresenter: MoviesCollectionViewDataSourceOutputs, GenresCollectionViewDataSourceOutputs {
    
    func didSelect(_ genre: Genre) {
        print(genre.id ?? 0)
        movieEntities.apiState.genreId = genre.id ?? 0
        movieEntities.apiState.genreName = genre.name ?? ""
        movieEntities.apiState.isFetching = true
        movieEntities.movies = []
        movieEntities.apiState.pageCount = 1
        dependencies.interactor.loadDiscoverMovieByGenre(genre: movieEntities.apiState.genreId, page: movieEntities.apiState.pageCount)
    }
    
    func didSelect(_ movie: Movie) {
        print("Movie ITEM CLICKED : \(movie.original_title)")
        let movieDetailEntity = MovieDetailEntities()
        movieDetailEntity.apiState.movieId = movie.id ?? 0
        dependencies.router.showMovieDetail(movieDetailEntities: movieDetailEntity)
    }
    
}
