//
//  GenresView.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

protocol GenresViewInputs: AnyObject {
    func configure(entities: GenreEntities)
    func reloadCollectionView(collectionViewDataSource: GenresCollectionViewDataSource)
    func indicatorView(animate: Bool)
}

protocol GenresViewOutputs: AnyObject {
    func viewDidLoad()
}
