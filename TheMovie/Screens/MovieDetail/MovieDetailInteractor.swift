//
//  MovieDetailInteractor.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import RxSwift

protocol MovieDetailInteractorOutputs: AnyObject {
    func onSuccess(data: MovieDetail)
    func onSuccessVideo(data: [MovieVideo]?)
    func onError(error: Error)
}

final class MovieDetailInteractor: Interactorable {
    /*
     Interactor ==> Class untuk mengolah data, 
     */
    weak var presenter: MovieDetailInteractorOutputs?
    let disposeBag = DisposeBag()

       func loadMovieDetail(movieID:Int) {
           provider.rx.request(.movieDetail(movieID))
               .subscribe { (event) in
                   switch event {
                   case .success(let result):
                       do {
                        self.loadMovieVideo(movieID: movieID)
                           let response = try JSONDecoder().decode(MovieDetail.self, from: result.data)
                           self.presenter?.onSuccess(data: response)
                       }
                       catch {
                           print(error.localizedDescription)
                        self.presenter?.onError(error: error)
                       }
                   case .error(let error):
                       print(error.localizedDescription)
                      self.presenter?.onError(error: error)
                   }
           }
           .disposed(by: disposeBag)
       }
    
    func loadMovieVideo(movieID:Int) {
        provider.rx.request(.movieVideos(movieID))
            .subscribe { (event) in
                switch event {
                case .success(let result):
                    do {
                        if let JSONString = String(data: result.data, encoding: String.Encoding.utf8) {
                            print(JSONString)
                        }
                        let response = try JSONDecoder().decode(MovieVideoResponse.self, from: result.data)
                        self.presenter?.onSuccessVideo(data: response.results ?? [])
                    }
                    catch {
                        print(error.localizedDescription)
                     self.presenter?.onError(error: error)
                    }
                case .error(let error):
                    print(error.localizedDescription)
                   self.presenter?.onError(error: error)
                }
        }
        .disposed(by: disposeBag)
    }
    
}
