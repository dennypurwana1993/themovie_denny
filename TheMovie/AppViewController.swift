//
//  AppViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/25/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class AppViewController: UIViewController {

    @IBOutlet weak var tmdbBtn: UIButton!
    @IBOutlet weak var imdbBtn: UIButton!
    @IBAction func imdbBtnTapped(_ sender: Any) {
        UserDefaults.standard.set("IMDB", forKey: "movie")
        showMainViewController()
    }
    @IBAction func tmdbBtnTapped(_ sender: Any) {
        UserDefaults.standard.set("TMDB", forKey: "movie")
        showMainViewController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
    }
    
    func initView(){
        
        tmdbBtn.backgroundColor = .clear
        tmdbBtn.layer.cornerRadius = 5
        tmdbBtn.layer.borderWidth = 1
        tmdbBtn.layer.borderColor = UIColor().colorFromHexString("#025BAB").cgColor
        
        imdbBtn.backgroundColor = .clear
        imdbBtn.layer.cornerRadius = 5
        imdbBtn.layer.borderWidth = 1
        imdbBtn.layer.borderColor = UIColor().colorFromHexString("#025BAB").cgColor
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
