//
//  MovieDetailImdbView.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/25/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import Foundation

protocol MovieDetailImdbViewInputs: AnyObject {
   
    func configure(entities: MovieDetailImdEntities)
    func indicatorView(animate: Bool)
    
}

protocol MovieDetailImdbViewOutputs: AnyObject {
    func viewDidLoad()
}
