//
//  Reviews.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

struct Reviews: Codable {
    let page : Int?
    let results: [Review]?
}

struct Review: Codable {
    
    let id: String?
    let author: String?
    let author_details: AuthorDetails?
    let content: String?
    let created_at: String?
    let updated_at: String?
    let url: String?

}

struct AuthorDetails: Codable {
    let name : String?
    let username: String?
    let avatar_path: String?
    let rating: Int?
}
