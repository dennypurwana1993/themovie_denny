//
//  MovieTop250Presenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/24/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

typealias MovieTop250PresenterDependencies = (
    interactor: MovieTop250Interactor,
    router: MovieTop250Output
)

final class MovieTop250Presenter: Presenterable {

    internal var entities: MovieTop250Entities!
    private weak var view: MovieTop250ViewInputs!
    let dependencies: MovieTop250PresenterDependencies

    init(entities: MovieTop250Entities,
         view: MovieTop250ViewInputs,
         dependencies: MovieTop250PresenterDependencies)
    {
        self.view = view
        self.entities = entities
        self.dependencies = dependencies
    }

}

extension MovieTop250Presenter: MovieTop250ViewOutputs {
    func onReachBottom() {

    }
    
    func viewDidLoad() {
        view.configure(entities: entities)
        dependencies.interactor.loadMovieTOP250()
    }

}

extension MovieTop250Presenter: MovieTop250InteractorOutputs {
    func onSuccess(datas: [MovieTop250]?) {
        var topRatedMovies = [MovieTop250]()
        var otherMovies = [MovieTop250]()
        for movie in datas ?? []{
            let rankInt = Int(movie.rank ?? "0")
            if(rankInt ?? 0 > 0 && rankInt ?? 0 <= 10) {
                topRatedMovies.append(movie)
            } else if(rankInt ?? 0 > 10 && rankInt ?? 0 <= 20)  {
                otherMovies.append(movie)
            }
        }
        entities.movies = ["topRated" : topRatedMovies, "otherMovie": otherMovies]
        view.reloadCollectionView(collectionViewDataSource: MovieTop250CollectionViewDataSource(entities: entities, presenter: self))
    }
    
    func onError(error: Error) {
        view.indicatorView(animate: false)
    }
    
}

extension MovieTop250Presenter: MovieTop250CollectionViewDataSourceOutputs {
   
    func didSelect(_ movie: MovieTop250) {
        print("Movie ITEM CLICKED : \(movie.title)")
        let movieDetailEntity = MovieDetailImdEntities(movie: movie)
        dependencies.router.showMovieDetail(movieDetailEntities: movieDetailEntity)
    }
    
}
