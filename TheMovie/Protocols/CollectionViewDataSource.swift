//
//  CollectionViewDataSource.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

protocol CollectionViewDataSource: AnyObject {
   
    var numberOfItems: Int { get }
    func getNumberOfItems(key: String) -> Int
    func itemCell(key: String, collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell
    func itemCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell
    func didSelect(collectionView: UICollectionView, indexPath: IndexPath)
    func didSelect(key: String, collectionView: UICollectionView, indexPath: IndexPath)
    
}
