//
//  DashboardViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/11/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var seeAllMovieLbl: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var movieCollectionView: UICollectionView! {
        didSet {
            movieCollectionView.delegate = self
            movieCollectionView.dataSource = self
            movieCollectionView.register(UINib.init(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "movieCell")
            
        }
    }
    
    @IBOutlet weak var genreCollectionView: UICollectionView! {
        didSet {
            genreCollectionView.delegate = self
            genreCollectionView.dataSource = self
            genreCollectionView.register(UINib.init(nibName: "GenreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "genreCell")
            
        }
    }
    
    internal var presenter: DashboardViewOutputs?
    internal var mCollectionViewDataSource: CollectionViewDataSource?
    internal var gCollectionViewDataSource: CollectionViewDataSource?
    var movieEntity = MovieEntities()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension DashboardViewController: DashboardViewInputs {
    
    func configure(genreEntities: GenreEntities, movieEntities: MovieEntities) {
        navigationItem.title = "Movie"
        let gestureBtnAllMovie = UITapGestureRecognizer(target: self, action:  #selector(self.showAllMovieByGenre))
        seeAllMovieLbl.isUserInteractionEnabled = true
        seeAllMovieLbl.addGestureRecognizer(gestureBtnAllMovie)
        
    }
    
    @objc func showAllMovieByGenre(){
        presenter?.showAllMoviesByGenre()
    }
    
    
    func reloadMovieCollectionView(mCollectionViewDataSource: MoviesCollectionViewDataSource) {
        self.mCollectionViewDataSource = mCollectionViewDataSource
        DispatchQueue.main.async { [weak self] in
            self?.movieCollectionView.reloadData()
        }
    }
    
    func reloadGenreCollectionView(gCollectionViewDataSource: GenresCollectionViewDataSource) {
        self.gCollectionViewDataSource = gCollectionViewDataSource
        DispatchQueue.main.async { [weak self] in
            self?.genreCollectionView.reloadData()
        }
    }
    
    func indicatorView(animate: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.genreCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: animate ? 50 : 0, right: 0)
            _ = animate ? self?.activityIndicator.startAnimating() : self?.activityIndicator.stopAnimating()
        }
    }
}

extension DashboardViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == genreCollectionView {
            return gCollectionViewDataSource?.numberOfItems ?? 0
        } else {
            print(mCollectionViewDataSource?.numberOfItems)
            return mCollectionViewDataSource?.numberOfItems ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == genreCollectionView {
            return gCollectionViewDataSource?.itemCell(collectionView: collectionView, indexPath: indexPath) ?? UICollectionViewCell()
        } else {
            return mCollectionViewDataSource?.itemCell(collectionView: collectionView, indexPath: indexPath) ?? UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == genreCollectionView {
            return gCollectionViewDataSource?.didSelect(collectionView: collectionView, indexPath: indexPath) ?? ()
        } else {
            return mCollectionViewDataSource?.didSelect(collectionView: collectionView, indexPath: indexPath) ?? ()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == genreCollectionView {
            let width = collectionView.bounds.width / 2
            return CGSize(width: width, height: 42 )
        } else {
            return CGSize(width: (collectionView.bounds.width), height: 250 )
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print("WILL DISPLAY \(mCollectionViewDataSource?.numberOfItems)")
        print("INDEXPath \(indexPath.row)")
        if collectionView == movieCollectionView {
            print("MOVIE COLLECTIONVIEW \((mCollectionViewDataSource?.numberOfItems ?? 0) - 1)")
            if(indexPath.row == (mCollectionViewDataSource?.numberOfItems ?? 0)-1){
                print("LAST INDEX == > LOAD MORE")
                updateNextSet()
            }
        }
    }
    
    func updateNextSet(){
        print("On Completetion")
        DispatchQueue.main.async { [weak self] in
            self?.presenter?.onReachBottom()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}

extension DashboardViewController: Viewable {}
