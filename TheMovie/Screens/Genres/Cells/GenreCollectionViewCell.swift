//
//  GenreCollectionViewCell.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class GenreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleBackground: UIView!
    @IBOutlet weak var titleGenre: UILabel!
    var widthLbl : CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with category: Genre) {
        
        var concatName = ""
        
        if(category.name!.count > 25){
            concatName = category.name!.prefix(25) + "..."
        } else {
            concatName = category.name!
        }
        
        titleGenre.text = concatName
        titleBackground.layer.cornerRadius = 20
        
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame  = layoutAttributes.frame
        frame.size.height = ceil(size.height)
        frame.size.width = titleGenre.intrinsicContentSize.width + 40
        layoutAttributes.frame = frame
        return layoutAttributes
    }
    
}
