//
//  MovieTop250Entities.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/24/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
final class MovieTop250Entities {
    
    var movies: [String: [MovieTop250]?] = [:]
    
    class MovieTop250ApiState {
        
    }
    
    var apiState = MovieTop250ApiState()
}
