//
//  ReviewsView.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

protocol ReviewsViewInputs: AnyObject {
    func configure(entities: ReviewsEntities)
    func reloadCollectionView(collectionViewDataSource: ReviewsCollectionViewDataSource)
    func indicatorView(animate: Bool)
}

protocol ReviewsViewOutputs: AnyObject {
    func viewDidLoad()
}
