//
//  ReviewsRouter.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

import UIKit

struct ReviewsRouterInput {

     func view(entryEntity: ReviewsEntities) -> ReviewsViewController {
        let view = ReviewsViewController()
        let mInteractor = ReviewsInteractor()
        let dependencies = ReviewsPresenterDependencies(interactor: mInteractor, router: ReviewsRouterOutput(view))
        let presenter = ReviewsPresenter(entities: entryEntity,  view: view, dependencies: dependencies)
        view.presenter = presenter
        view.reviewsCollectionViewDataSource = ReviewsCollectionViewDataSource(entities: presenter.entities, presenter: presenter)
        mInteractor.presenter = presenter
        return view
    }

    func push(from: Viewable, entryEntity: ReviewsEntities) {
        print("PUSH VIEW CONTROLLER")
        let view = self.view(entryEntity: entryEntity)
        from.push(view, animated: true)
    }

    func present(from: Viewable, entryEntity: ReviewsEntities) {
        let nav = UINavigationController(rootViewController: view(entryEntity: entryEntity))
        from.present(nav, animated: true)
    }
}

final class ReviewsRouterOutput: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }
}
