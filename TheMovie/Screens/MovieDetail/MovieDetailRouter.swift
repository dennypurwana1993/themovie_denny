//
//  MovieDetailRouter.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

struct MovieDetailRouterInput {

    private func view(entryEntity: MovieDetailEntities) -> MovieDetailViewController {
        let view = MovieDetailViewController()
        let interactor = MovieDetailInteractor()
        let mInteractor = ReviewsInteractor()
        let dependencies2 = MovieDetailPresenterDependencies2(interactor: mInteractor, router: MovieDetailRouterOutput(view))
        let reviewEntity = ReviewsEntities()
        reviewEntity.apiState.movieId = entryEntity.apiState.movieId
        let dependencies = MovieDetailPresenterDependencies(interactor: interactor, router: MovieDetailRouterOutput(view))
        let presenter = MovieDetailPresenter(movieDetailEntity: entryEntity,
                                              reviewEntity: reviewEntity,
                                              view: view,
                                              dependencies: dependencies,
                                              dependencies2: dependencies2)
        view.presenter = presenter
        view.reviewsCollectionViewDataSource = ReviewsCollectionViewDataSource(entities: reviewEntity, presenter: presenter)
        interactor.presenter = presenter
        mInteractor.presenter = presenter
        return view
    }

    func push(from: Viewable, entryEntity: MovieDetailEntities) {
        print("PUSH VIEW CONTROLLER")
        let view = self.view(entryEntity: entryEntity)
        from.push(view, animated: true)
    }

    func present(from: Viewable, entryEntity: MovieDetailEntities) {
        let nav = UINavigationController(rootViewController: view(entryEntity: entryEntity))
        from.present(nav, animated: true)
    }
}

final class MovieDetailRouterOutput: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }
}
