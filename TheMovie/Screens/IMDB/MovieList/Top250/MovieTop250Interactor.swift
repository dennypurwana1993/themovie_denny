//
//  MovieTop250Interactor.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/24/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import RxSwift
//MovieTop250Response
protocol MovieTop250InteractorOutputs: AnyObject {
    func onSuccess(datas: [MovieTop250]?)
    func onError(error: Error)
}

final class MovieTop250Interactor: Interactorable {
    /* call movies top 250 api*/
    weak var presenter: MovieTop250InteractorOutputs?
    let disposeBag = DisposeBag()
    func loadMovieTOP250() {
        provider.rx.request(.movieTop250)
            .subscribe { (event) in
                switch event {
                case .success(let result):
                    do {
                        if let JSONString = String(data: result.data, encoding: String.Encoding.utf8) {
                            print(JSONString)
                        }
                        let response = try JSONDecoder().decode(MovieTop250Response.self, from: result.data)
                        self.presenter?.onSuccess(datas: response.items)
                    }
                    catch {
                        print(error.localizedDescription)
                        self.presenter?.onError(error: error)
                    }
                case .error(let error):
                    print(error.localizedDescription)
                    self.presenter?.onError(error: error)
                }
        }
        .disposed(by: disposeBag)
    }
    
    
}
