//
//  ReviewsPresenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.


import Foundation

typealias ReviewsPresenterDependencies = (
    interactor: ReviewsInteractor,
    router: ReviewsRouterOutput
)

final class ReviewsPresenter: Presenterable {

    internal var entities: ReviewsEntities
    private weak var view: ReviewsViewInputs!
    let dependencies: ReviewsPresenterDependencies

    init(entities: ReviewsEntities,
         view: ReviewsViewInputs,
         dependencies: ReviewsPresenterDependencies)
    {
        self.view = view
        self.entities = entities
        self.dependencies = dependencies
    }

}

extension ReviewsPresenter: ReviewsViewOutputs {
    func viewDidLoad() {
        view.configure(entities: entities)
        entities.apiState.isFetching = true
        dependencies.interactor.loadAllReviews(movieID: entities.apiState.movieId, page: entities.apiState.pageCount)
    }

}

extension ReviewsPresenter: ReviewsInteractorOutputs {
    func onSuccess(datas: Reviews) {
        entities.apiState.isFetching = false
        entities.apiState.pageCount += 1
        entities.reviews += datas.results ?? []
        view.reloadCollectionView(collectionViewDataSource: ReviewsCollectionViewDataSource(entities: entities, presenter: self))
    }
    
    func onError(error: Error) {
        view.indicatorView(animate: false)
    }
    
}

extension ReviewsPresenter: ReviewsCollectionViewDataSourceOutputs {
    func didSelect(_ review: Review) {
        
    }
}

