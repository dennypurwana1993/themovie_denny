//
//  ReviewsInteractor.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import RxSwift

protocol ReviewsInteractorOutputs: AnyObject {
    func onSuccess(datas: Reviews)
    func onError(error: Error)
}

final class ReviewsInteractor: Interactorable {

    weak var presenter: ReviewsInteractorOutputs?
    let disposeBag = DisposeBag()
    
    func loadAllReviews(movieID:Int, page: Int) {
        print("movie id reviews \(movieID) page : \(page)")
        provider.rx.request(.movieReview(movieID, page))
                .subscribe { (event) in
                    switch event {
                    case .success(let result):
                        do {
                            if let JSONString = String(data: result.data, encoding: String.Encoding.utf8) {
                                print("REVIEWS DATA")
                                print(JSONString)
                                print("==================")
                            }
                            let response = try JSONDecoder().decode(Reviews.self, from: result.data)
                            self.presenter?.onSuccess(datas: response)
                        }
                        catch {
                            print(error.localizedDescription)
                             self.presenter?.onError(error: error)
                        }
                    case .error(let error):
                        print(error.localizedDescription)
                         self.presenter?.onError(error: error)
                    }
            }
            .disposed(by: disposeBag)
        }
    
}
