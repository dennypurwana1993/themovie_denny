//
//  MoviesRouter.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

struct MoviesRouterInput {

    private func view(entryEntity: MovieEntities) -> MoviesViewController {
        let view = MoviesViewController()
        let interactor = MoviesInteractor()
        let dependencies = MoviesPresenterDependencies(interactor: interactor, router: MoviesRouterOutput(view))
        let presenter = MoviesPresenter(entities: entryEntity, view: view, dependencies: dependencies)
        view.presenter = presenter
        view.collectionViewDataSource = MoviesCollectionViewDataSource(entities: presenter.entities, presenter: presenter)
        interactor.presenter = presenter
        return view
    }

    func push(from: Viewable, entryEntity: MovieEntities) {
        print("PUSH VIEW CONTROLLER")
        let view = self.view(entryEntity: entryEntity)
        from.push(view, animated: true)
    }

    func present(from: Viewable, entryEntity: MovieEntities) {
        let nav = UINavigationController(rootViewController: view(entryEntity: entryEntity))
        from.present(nav, animated: true)
    }
}

final class MoviesRouterOutput: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }
    
    func showMovieDetail(movieDetailEntities: MovieDetailEntities) {
        print("Show movie detail")
        MovieDetailRouterInput().push(from: view, entryEntity: movieDetailEntities)
    }
}
