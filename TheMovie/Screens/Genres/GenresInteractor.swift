//
//  GenresInteractor.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import RxSwift

protocol GenresInteractorOutputs: AnyObject {
    func onSuccess(datas: Genres)
    func onError(error: Error)
}

final class GenresInteractor: Interactorable {

    weak var presenter: GenresInteractorOutputs?
    let disposeBag = DisposeBag()
    
    func loadGenres() {
        provider.rx.request(.genres)
            .subscribe { (event) in
                switch event {
                case .success(let result):
                    do {
                        if let JSONString = String(data: result.data, encoding: String.Encoding.utf8) {
                            print(JSONString)
                        }
                        let response = try JSONDecoder().decode(Genres.self, from: result.data)
                        self.presenter?.onSuccess(datas: response)
                    }
                    catch {
                        print(error.localizedDescription)
                        self.presenter?.onError(error: error)
                    }
                case .error(let error):
                    print(error.localizedDescription)
                    self.presenter?.onError(error: error)
                }
        }
        .disposed(by: disposeBag)
    }
    
}
