//
//  Services.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit
import Foundation
import Moya

let kTmdbApiKey = "f19f233a1b89adcd11fb98cab0f257ba"
let kIMDBApiKey = "k_ly652rz2"

let provider = MoyaProvider<Services>()

enum Services {
    ///parameters: page
    case discoverMovie(Int,Int)
    case genres
    case popular
    case movieDetail(Int)
    case movieVideos(Int)
    case movieReview(Int,Int)
    case upload(UIImage)
    case movieTop250
}

enum MovieError: Error, CustomNSError {
    
    case apiError
    case invalidEndpoint
    case invalidResponse
    case noData
    case serializationError
    
    var localizedDescription: String {
        switch self {
        case .apiError: return "Failed to fetch data"
        case .invalidEndpoint: return "Invalid endpoint"
        case .invalidResponse: return "Invalid response"
        case .noData: return "No data"
        case .serializationError: return "Failed to decode data"
        }
    }
    
    var errorUserInfo: [String : Any] {
        [NSLocalizedDescriptionKey: localizedDescription]
    }
    
}

extension Services: TargetType  {
    
    var baseURL: URL {
        if(UserDefaults.standard.string(forKey: "movie") == "TMDB"){
            return URL(string: Config.BaseUrl)!
        } else {
            return URL(string: Config.BaseUrlImdb)!
        }
//        #if (DEV)
//        return URL(string: Config.BaseUrl)!
//        #else
//        return URL(string: Config.BaseUrl)!
//        #endif
    }
    
    var path: String {
        
        switch self {
        case .discoverMovie:
            return "/discover/movie"
            
        case .genres:
            return "/genre/movie/list"
            
        case .popular:
            return "/movie/popular"
            
        case .movieDetail(let movieId):
            return "/movie/\(movieId)"
            
        case .movieReview(let movieId, _):
            return "/movie/\(movieId)/reviews"
            
        case .movieVideos(let movieId):
            return "/movie/\(movieId)/videos"
            
        case .movieTop250:
            return "/en/API/Top250Movies/\(kIMDBApiKey)"
            
        case .upload:
            return "/upload"
        }
    }
    
    var method: Moya.Method {
        
        switch self {
        case .upload:
            return .post
            
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        
        case .discoverMovie(let genre, let page):
            let parameters: [String: Any] = [
                "page" : page,
                "api_key": kTmdbApiKey,
                "with_genres": genre
            ]
            return .requestParameters(
                parameters: parameters,
                encoding: URLEncoding.default
            )
            
            
        case .upload(let image):
            if let data = image.pngData() {
                let form = MultipartFormData.init(provider: .data(data), name: "image", fileName: "image.png", mimeType: "image/png")
                return .uploadMultipart([form])
            }
            return .requestPlain
            
        case .genres:
            let parameters: [String: Any] = [
                "api_key": kTmdbApiKey
            ]
            return .requestParameters(
                parameters: parameters,
                encoding: URLEncoding.default
            )
            
        case .popular:
            let parameters: [String: Any] = [
                "api_key": kTmdbApiKey
            ]
            return .requestParameters(
                parameters: parameters,
                encoding: URLEncoding.default
            )
            
        case .movieDetail(_):
            let parameters: [String: Any] = [
                "api_key": kTmdbApiKey
            ]
            return .requestParameters(
                parameters: parameters,
                encoding: URLEncoding.default
            )
            
        case .movieVideos(_):
            let parameters: [String: Any] = [
                "api_key": kTmdbApiKey
            ]
            return .requestParameters(
                parameters: parameters,
                encoding: URLEncoding.default
            )
            
        case .movieReview(_, let page):
            let parameters: [String: Any] = [
                "api_key": kTmdbApiKey,
                "language" : "en-US",
                "page" : page
            ]
            return .requestParameters(
                parameters: parameters,
                encoding: URLEncoding.default
            )
         
        case .movieTop250:
            let parameters: [String: Any] = [:]
            return .requestParameters(
                parameters: parameters,
                encoding: URLEncoding.default
            )
            
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
}
