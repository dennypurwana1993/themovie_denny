//
//  MovieDetailPresenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

typealias MovieDetailPresenterDependencies = (
    interactor: MovieDetailInteractor,
    router: MovieDetailRouterOutput
)

typealias MovieDetailPresenterDependencies2 = (
    interactor: ReviewsInteractor,
    router: MovieDetailRouterOutput
)

/*
 Presenter ==> Jembatan antara Interactor(pengolah data) dan view (tampilan muka atau interface)
 */

final class MovieDetailPresenter: Presenterable {

    internal var movieDetailEntities: MovieDetailEntities!
    private weak var view: MovieDetailViewInputs!
    let dependencies: MovieDetailPresenterDependencies
    internal var reviewEntities: ReviewsEntities!
    let dependencies2: MovieDetailPresenterDependencies2
    
   
    init(movieDetailEntity: MovieDetailEntities,
         reviewEntity: ReviewsEntities,
         view: MovieDetailViewInputs,
         dependencies: MovieDetailPresenterDependencies,
         dependencies2: MovieDetailPresenterDependencies2)
    {
        self.view = view
        self.movieDetailEntities = movieDetailEntity
        self.reviewEntities = reviewEntity
        self.dependencies = dependencies
        self.dependencies2 = dependencies2
    }

}

extension MovieDetailPresenter: MovieDetailViewOutputs {
    func viewDidLoad() {
        self.dependencies.interactor.loadMovieDetail(movieID: self.movieDetailEntities.apiState.movieId)
        self.dependencies2.interactor.loadAllReviews(movieID: self.reviewEntities.apiState.movieId, page: self.reviewEntities.apiState.pageCount)
    }

}

extension MovieDetailPresenter: MovieDetailInteractorOutputs, ReviewsInteractorOutputs {
    
    func onSuccess(datas: Reviews) {
        reviewEntities.apiState.isFetching = false
        reviewEntities.apiState.pageCount += 1
        reviewEntities.reviews += datas.results ?? []
        view.reloadCollectionView(collectionViewDataSource: ReviewsCollectionViewDataSource(entities: reviewEntities, presenter: self))
    }
    
    func onSuccessVideo(data: [MovieVideo]?) {
        view.showYoutubeVideo(datas: data ?? [])
    }
    
    func onSuccess(data: MovieDetail) {
        print("OUTPUT MOVIE DETAIL DATA : \(data.overview ?? "")")
        view.configure(data: data)
    }
    
    func onError(error: Error) {
        view.indicatorView(animate: false)
    }
    
}

extension MovieDetailPresenter: MoviesCollectionViewDataSourceOutputs {
   
    func didSelect(_ movie: Movie) {
        print("Movie ITEM CLICKED : \(movie.name)")
    }
    
}

extension MovieDetailPresenter: ReviewsCollectionViewDataSourceOutputs {
    func didSelect(_ review: Review) {
        
    }
}
