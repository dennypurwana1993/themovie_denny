//
//  Config.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

import UIKit

enum Config {
   
    static let BaseUrl : String = "https://api.themoviedb.org/3"
    static let BaseUrlImages : String = "https://image.tmdb.org/t/p/w500"
    static let BaseUrlImdb : String = "https://imdb-api.com"
    static let BaseUrlImdbImages : String = "https://image.tmdb.org/t/p/w500"

}

enum Color {
    
    static let primaryColor =  UIColor().colorFromHexString("#4CABB8").cgColor
    static let secondaryColor = UIColor().colorFromHexString("#025BAB").cgColor
    static let backgroundColor = UIColor().colorFromHexString("#F2F2F2").cgColor
    static let borderInActiveColor = UIColor().colorFromHexString("#ECECEC").cgColor
    static let borderActiveColor = UIColor().colorFromHexString("#005BAA").cgColor
    
}

