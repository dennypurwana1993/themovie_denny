//
//  MoviesInteractor.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import RxSwift

protocol MoviesInteractorOutputs: AnyObject {
    func onSuccess(datas: Movies)
    func onError(error: Error)
}

final class MoviesInteractor: Interactorable {
    /* call movies api*/
    weak var presenter: MoviesInteractorOutputs?
    let disposeBag = DisposeBag()
    func loadDiscoverMovieByGenre(genre:Int, page: Int) {
        provider.rx.request(.discoverMovie(genre,page))
            .subscribe { (event) in
                switch event {
                case .success(let result):
                    do {
                        if let JSONString = String(data: result.data, encoding: String.Encoding.utf8) {
                            print(JSONString)
                        }
                        let response = try JSONDecoder().decode(Movies.self, from: result.data)
                        self.presenter?.onSuccess(datas: response)
                    }
                    catch {
                        print(error.localizedDescription)
                        self.presenter?.onError(error: error)
                    }
                case .error(let error):
                    print(error.localizedDescription)
                    self.presenter?.onError(error: error)
                }
        }
        .disposed(by: disposeBag)
    }
    
    
}
