//
//  MovieDetailEntities.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
final class MovieDetailEntities {
    
    class MovieDetailApiState {
        var movieId = 0
    }
    
    var apiState = MovieDetailApiState()
}
