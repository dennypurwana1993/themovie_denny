//
//  MoviesCollectionViewDataSource.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

protocol MoviesCollectionViewDataSourceOutputs: AnyObject {
    func didSelect(_ movie: Movie)
}

final class MoviesCollectionViewDataSource: CollectionViewDataSource {
   
    func didSelect(key: String, collectionView: UICollectionView, indexPath: IndexPath) {
        
    }
  
    func itemCell(key: String, collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
    
    func getNumberOfItems(key: String) -> Int {
        return 0
    }
    
    
    private weak var entities: MovieEntities!
    private weak var presenter: MoviesCollectionViewDataSourceOutputs?
   
    init(entities: MovieEntities, presenter: MoviesCollectionViewDataSourceOutputs) {
        self.entities = entities
        self.presenter = presenter
    }
    
    var numberOfItems: Int {
        return entities.movies.count
    }
    
    func itemCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let movie = entities.movies[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        cell.configure(with: movie)
        return cell
    }
    
    func didSelect(collectionView: UICollectionView, indexPath: IndexPath) {
       
        let selectedMovie = entities.movies[indexPath.row]
        presenter?.didSelect(selectedMovie)
        
    }
    
}
