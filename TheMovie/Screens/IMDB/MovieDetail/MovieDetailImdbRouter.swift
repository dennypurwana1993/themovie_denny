//
//  MovieDetailImdbRouter.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/25/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

struct MovieDetailImdbRouterInput {

    func view(entryEntity: MovieDetailImdEntities) -> MovieDetailImdbViewController {
        let view = MovieDetailImdbViewController()
        let interactor = MovieDetailImdbInteractor()
        let dependencies = MovieDetailImdbPresenterDependencies(interactor: interactor, router: MovieDetailImdbRouterOutput(view))
        print("ENTRY ENTITY \(entryEntity.movie.fullTitle ?? "")")
        let presenter = MovieDetailImdbPresenter(entities: entryEntity,  view: view, dependencies: dependencies)
        view.presenter = presenter
        return view
    }

    func push(from: Viewable, entity: MovieDetailImdEntities) {
        print("PUSH VIEW CONTROLLER")
        let view = self.view(entryEntity: entity)
        from.push(view, animated: true)
    }

    func present(from: Viewable, entity: MovieDetailImdEntities) {
        let nav = UINavigationController(rootViewController: view(entryEntity: entity))
        from.present(nav, animated: true)
    }
}

final class MovieDetailImdbRouterOutput: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }
    
}
