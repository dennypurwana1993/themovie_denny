//
//  MovieDetailViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit
import YouTubePlayer
class MovieDetailViewController: UIViewController {
    @IBOutlet weak var reviewsCollectionView: UICollectionView! {
        didSet {
            reviewsCollectionView.delegate = self
            reviewsCollectionView.dataSource = self
            reviewsCollectionView.register(UINib.init(nibName: "ReviewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "reviewsCell")

        }
    }
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var releasedView: UIView!
    @IBOutlet weak var seeAllReviewsLbl: UILabel!
    @IBOutlet weak var ytPlayerView: YouTubePlayerView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var overviewLbl: UILabel!
    @IBOutlet weak var releaseDateLbl: UILabel!
    @IBOutlet weak var releaseStatusLbl: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    var movieId = 0
    var movieTitle = ""
    internal var reviewsCollectionViewDataSource: CollectionViewDataSource?
    internal var presenter: MovieDetailViewOutputs?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
}


extension MovieDetailViewController: MovieDetailViewInputs {
    
    func reloadCollectionView(collectionViewDataSource: ReviewsCollectionViewDataSource) {
        self.reviewsCollectionViewDataSource = collectionViewDataSource
        DispatchQueue.main.async { [weak self] in
            self?.reviewsCollectionView.reloadData()
        }
    }
    
    func showYoutubeVideo(datas: [MovieVideo]?) {
        if(datas?.count ?? 0 > 0){
        let keyVideo = datas?[0].key ?? ""
        let myVideoURL = NSURL(string: "https://youtu.be/\(keyVideo)")
        ytPlayerView.playerVars = [
            "playsinline": "1",
            "controls": "0",
            "showinfo": "0"
            ] as YouTubePlayerView.YouTubePlayerParameters
            ytPlayerView.loadVideoURL(myVideoURL! as URL)
            
        }
    }
    
    func indicatorView(animate: Bool) {
    }
    
    func configure(data: MovieDetail) {
        navigationItem.title = "Movie"
        self.releasedView.layer.cornerRadius = 5
        self.releasedView.clipsToBounds = true
        self.movieId = data.id ?? 0
        self.movieTitle = data.title ?? ""
        var genreArr : [String] = []
        for genre in (data.genres ?? []) {
            genreArr.append("\(genre.name ?? "") | ")
        }
        let genreVal = genreArr.joined(separator: "")
        ratingLbl.text = "\(data.vote_average ?? 7.0)/10 "
        genreLbl.text = genreVal
        titleLbl.text = data.original_title ?? ""
        overviewLbl.text = data.overview ?? ""
        releaseDateLbl.text = data.release_date ?? ""
        releaseStatusLbl.text = data.status ?? ""
        let imageUrlBackdrop =  "\(Config.BaseUrlImages)\(data.backdrop_path ?? "")"
        posterImageView.kf.setImage(with: URL(string: imageUrlBackdrop), placeholder: UIImage(named: "clapperboard"))
        let gestureBtnReviews = UITapGestureRecognizer(target: self, action:  #selector(self.showAllReviews))
        seeAllReviewsLbl.isUserInteractionEnabled = true
        seeAllReviewsLbl.addGestureRecognizer(gestureBtnReviews)
        
    }
    
    @objc func showAllReviews(){
        let reviewEntity = ReviewsEntities()
        reviewEntity.apiState.movieId = self.movieId
        reviewEntity.apiState.movieTitle = self.movieTitle
        ReviewsRouterInput().push(from: self, entryEntity: reviewEntity)
    }
    
    func showReviews(review: ReviewsEntities) {
        
    }
        
}


extension MovieDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return reviewsCollectionViewDataSource?.numberOfItems ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
            return reviewsCollectionViewDataSource?.itemCell(collectionView: collectionView, indexPath: indexPath) ?? UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            return reviewsCollectionViewDataSource?.didSelect(collectionView: collectionView, indexPath: indexPath) ?? ()
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: (collectionView.bounds.width), height: 250 )
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}

extension MovieDetailViewController: Viewable {}
