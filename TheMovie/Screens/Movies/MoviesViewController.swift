//
//  MoviesViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/11/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

final class MoviesViewController: UIViewController {

    internal var presenter: MoviesViewOutputs?
    internal var collectionViewDataSource: CollectionViewDataSource?

    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var movieCollectionView: UICollectionView! {
        didSet {
            movieCollectionView.delegate = self
            movieCollectionView.dataSource = self
            movieCollectionView.register(UINib.init(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "movieCell")

        }
    }
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

}

extension MoviesViewController: MoviesViewInputs {

    func configure(entities: MovieEntities) {
        navigationItem.title = "Movie"
        genreLbl.text = "Genre > \(entities.apiState.genreName)"
    }

    func reloadCollectionView(collectionViewDataSource: MoviesCollectionViewDataSource) {
        self.collectionViewDataSource = collectionViewDataSource
        DispatchQueue.main.async { [weak self] in
            self?.movieCollectionView.reloadData()
        }
    }

    func indicatorView(animate: Bool) {
        print("ANIMATE AI : \(animate)")
        DispatchQueue.main.async { [weak self] in
            self?.movieCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: animate ? 50 : 0, right: 0)
            _ = animate ? self?.indicatorView.startAnimating() : self?.indicatorView.stopAnimating()
        }
    }
}

extension MoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewDataSource?.numberOfItems ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionViewDataSource?.itemCell(collectionView: collectionView, indexPath: indexPath) ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        return collectionViewDataSource?.didSelect(collectionView: collectionView, indexPath: indexPath) ?? ()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: (collectionView.bounds.width), height: 250 )
       }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
         if(indexPath.row == (collectionViewDataSource?.numberOfItems ?? 0)-1){
                print("LAST INDEX == > LOAD MORE")
                updateNextSet()
            }
    }
    
    func updateNextSet(){
        print("On Completetion")
        DispatchQueue.main.async { [weak self] in
            self?.presenter?.onReachBottom()
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
    }
}

extension MoviesViewController: Viewable {}
