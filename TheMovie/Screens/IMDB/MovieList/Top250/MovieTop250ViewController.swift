//
//  MovieTop250ViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/24/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class MovieTop250ViewController: UIViewController {
    internal var presenter: MovieTop250ViewOutputs?
    internal var topRatedCollectionViewDataSource: CollectionViewDataSource?
    internal var gCollectionViewDataSource: CollectionViewDataSource?
    @IBOutlet weak var othersCollectionView: UICollectionView! {
        didSet {
            othersCollectionView.delegate = self
            othersCollectionView.dataSource = self
            othersCollectionView.register(UINib.init(nibName: "MovieOtherCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "otherCell")
            
        }
    }
    @IBOutlet weak var topRatedCollectionView: UICollectionView!{
        didSet {
            topRatedCollectionView.delegate = self
            topRatedCollectionView.dataSource = self
            topRatedCollectionView.register(UINib.init(nibName: "TopRatedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "topRatedCell")
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension MovieTop250ViewController: MovieTop250ViewInputs {
    func indicatorView(animate: Bool) {
        
    }
    
    func configure(entities: MovieTop250Entities) {
        
    }
    
    func reloadCollectionView(collectionViewDataSource: MovieTop250CollectionViewDataSource) {
        self.topRatedCollectionViewDataSource = collectionViewDataSource
        DispatchQueue.main.async { [weak self] in
            self?.topRatedCollectionView.reloadData()
            self?.othersCollectionView.reloadData()
        }
    }

}

extension MovieTop250ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        if(collectionView == self.topRatedCollectionView){
            return topRatedCollectionViewDataSource?.getNumberOfItems(key: "topRated") ?? 0
        } else {
            return topRatedCollectionViewDataSource?.getNumberOfItems(key: "otherMovie") ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == self.topRatedCollectionView){
            return topRatedCollectionViewDataSource?.itemCell(key: "topRated", collectionView: collectionView, indexPath: indexPath) ?? UICollectionViewCell()
        } else {
            return topRatedCollectionViewDataSource?.itemCell(key: "otherMovie", collectionView: collectionView, indexPath: indexPath) ?? UICollectionViewCell()
        }
            
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == self.topRatedCollectionView){
            
            return topRatedCollectionViewDataSource?.didSelect(key: "topRated", collectionView: collectionView, indexPath: indexPath) ?? ()
        } else {
            
            return topRatedCollectionViewDataSource?.didSelect(key: "otherMovie", collectionView: collectionView, indexPath: indexPath) ?? ()
        }
         
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == self.topRatedCollectionView){
            return CGSize(width: (collectionView.bounds.width / 3) + 40, height: collectionView.bounds.height )
        } else {
            return CGSize(width: (collectionView.bounds.width), height: collectionView.bounds.height / 3 )
        }
        
    }
}



extension MovieTop250ViewController: Viewable {}
