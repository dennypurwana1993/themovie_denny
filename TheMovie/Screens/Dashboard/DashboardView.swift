//
//  DashboardView.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit
protocol DashboardViewInputs: AnyObject {
   
    func configure(genreEntities: GenreEntities, movieEntities: MovieEntities)
    func reloadMovieCollectionView(mCollectionViewDataSource: MoviesCollectionViewDataSource)
    func reloadGenreCollectionView(gCollectionViewDataSource: GenresCollectionViewDataSource)
    func indicatorView(animate: Bool)
    
}

protocol DashboardViewOutputs: AnyObject {
    func viewDidLoad()
    func onReachBottom()
    func showAllMoviesByGenre()
}
