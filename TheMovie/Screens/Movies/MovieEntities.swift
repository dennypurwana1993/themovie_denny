//
//  MovieEntities.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
final class MovieEntities {
    var movies: [Movie] = []
    
    class MovieApiState {
        var pageCount = 1
        var isFetching = false
        var genreId = 28
        var genreName = ""
    }
    
    var apiState = MovieApiState()
}
