//
//  Movies.swift
//  TheMovie
//
// Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

struct Movies: Codable {
    let page : Int?
    let results: [Movie]?
    let total_pages: Int?
}

struct Movie: Codable {
    let id: Int?
    let adult: Bool?
    let name: String?
    let backdrop_path: String?
    let genre_ids: [Int]?
    let original_language: String?
    let original_title: String?
    let overview: String?
    let popularity: Double?
    let poster_path: String?
    let release_date: String?
    let title: String?
    let video: Bool?
    let vote_average: Double?
    let vote_count: Int?
}

struct MovieVideo: Codable {
    let iso_639_1: String?
    let iso_3166_1: String?
    let name: String?
    let key: String?
    let site: String?
    let size: Int?
    let type: String?
    let official: Bool?
    let published_at: String?
    let id: String?
}

struct MovieVideoResponse: Codable {
    
    let results: [MovieVideo]?
    let id: Int?
    
}

struct MovieTop250: Codable {
   
   let id: String?
   let rank: String?
   let title: String?
   let fullTitle: String?
   let year: String?
   let image: String?
   let crew: String?
   let imDbRating: String?
   let imDbRatingCount: String?
    
}

struct MovieTop250Response: Codable {
    
    let items: [MovieTop250]?

}
