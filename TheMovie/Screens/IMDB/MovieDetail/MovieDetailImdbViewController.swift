//
//  MovieDetailImdbViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/25/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class MovieDetailImdbViewController: UIViewController {
    
    
    @IBOutlet weak var crewLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var fullTitleLbl: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!

    internal var presenter: MovieDetailImdbViewOutputs?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

}

extension MovieDetailImdbViewController: MovieDetailImdbViewInputs {
    func configure(entities: MovieDetailImdEntities) {
        print("TEST ENTITY \(entities.movie.fullTitle ?? "")")
        self.fullTitleLbl.text = entities.movie.fullTitle ?? ""
        self.crewLbl.text = entities.movie.crew ?? ""
        self.yearLbl.text = entities.movie.year ?? ""
        self.ratingLbl.text = "\(entities.movie.imDbRating ?? "")/10"
        posterImageView.kf.setImage(with: URL(string: entities.movie.image ?? ""), placeholder: UIImage(named: "clapperboard"))
        posterImageView.makeRoundCorners(byRadius: 20)
    }
    
    func indicatorView(animate: Bool) {
        
    }
    
   
}
extension MovieDetailImdbViewController: Viewable {}
