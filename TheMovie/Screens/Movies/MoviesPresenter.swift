//
//  MoviesPresenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

typealias MoviesPresenterDependencies = (
    interactor: MoviesInteractor,
    router: MoviesRouterOutput
)

final class MoviesPresenter: Presenterable {

    internal var entities: MovieEntities!
    private weak var view: MoviesViewInputs!
    let dependencies: MoviesPresenterDependencies

    init(entities: MovieEntities,
         view: MoviesViewInputs,
         dependencies: MoviesPresenterDependencies)
    {
        self.view = view
        self.entities = entities
        self.dependencies = dependencies
    }

}

extension MoviesPresenter: MoviesViewOutputs {
    func onReachBottom() {
        print("ONREACH BOTTOM")
        print(entities.apiState.isFetching)
        print(entities.apiState.genreId)
        print(entities.apiState.pageCount)
//        guard !movieEntities.apiState.isFetching else { return }
//        movieEntities.apiState.isFetching = true
        dependencies.interactor.loadDiscoverMovieByGenre(genre: entities.apiState.genreId, page: entities.apiState.pageCount)
//        view.indicatorView(animate: true)
    }
    
    func viewDidLoad() {
       view.configure(entities: entities)
       entities.apiState.isFetching = true
        dependencies.interactor.loadDiscoverMovieByGenre(genre: entities.apiState.genreId, page: entities.apiState.pageCount)
    }

}

extension MoviesPresenter: MoviesInteractorOutputs {
    func onSuccess(datas: Movies) {
        entities.apiState.isFetching = false
        entities.apiState.pageCount += 1
        entities.movies += datas.results ?? []
        view.reloadCollectionView(collectionViewDataSource: MoviesCollectionViewDataSource(entities: entities, presenter: self))
    }
    
    func onError(error: Error) {
        view.indicatorView(animate: false)
    }
    
}

extension MoviesPresenter: MoviesCollectionViewDataSourceOutputs {
   
    func didSelect(_ movie: Movie) {
        print("Movie ITEM CLICKED : \(movie.name)")
        let movieDetailEntity = MovieDetailEntities()
        movieDetailEntity.apiState.movieId = movie.id ?? 0
        dependencies.router.showMovieDetail(movieDetailEntities: movieDetailEntity)
    }
    
}
