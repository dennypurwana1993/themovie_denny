//
//  MovieCollectionViewCell.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var overViewLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backdropImageView: UIImageView!

    @IBOutlet weak var releaseDateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with movie: Movie) {
      
        let imageUrlBackdrop =  "\(Config.BaseUrlImages)\(movie.backdrop_path ?? "")"
        backdropImageView.kf.setImage(with: URL(string: imageUrlBackdrop), placeholder: UIImage(named: "clapperboard"))
        titleLbl.text = movie.title
        releaseDateLbl.text = movie.release_date
        overViewLbl.text = movie.overview
        backdropImageView.makeRoundCorners(byRadius: 10)
        
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame  = layoutAttributes.frame
        //        frame.size.height = ceil(size.height)
        //        frame.size.width = ceil(size.width)
        layoutAttributes.frame = frame
        return layoutAttributes
    }
    
}
