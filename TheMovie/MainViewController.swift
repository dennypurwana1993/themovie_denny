//
//  MainViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView(){
        if(UserDefaults.standard.string(forKey: "movie") == "IMDB"){
            let vc: UIViewController = MovieTop250RouterInput().view()
            addChild(vc)
            containerView.addSubview(vc.view)
            vc.didMove(toParent: self)
        } else {
            
            let vc: UIViewController = DashboardRouterInput().view()
            addChild(vc)
            containerView.addSubview(vc.view)
            vc.didMove(toParent: self)
            
        }
       }
    
}


extension UIViewController {
    
    func showMainViewController() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "main") as! MainViewController
        vc.modalPresentationStyle = .fullScreen
        vc.hidesBottomBarWhenPushed = true
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        present(vc, animated: false, completion: nil)
        
    }
}


extension MainViewController: Viewable {}
