//
//  MovieDetailImdEntities.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/25/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

final class MovieDetailImdEntities {
    
    var movie: MovieTop250
    
    init(movie: MovieTop250) {
        self.movie = movie
    }
    
    class MovieTop250ApiState {
        
    }
    
    var apiState = MovieTop250ApiState()
}
