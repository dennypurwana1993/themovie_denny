//
//  DashboardRouter.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/11/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

struct DashboardRouterInput {

     func view() -> DashboardViewController {
        let view = DashboardViewController()
        let mInteractor = MoviesInteractor()
        let gInteractor = GenresInteractor()
        let dependencies = DashboardPresenterDependencies(interactor: mInteractor, router: DashboardRouterOutput(view))
        let dependencies2 = DashboardPresenterDependencies2(interactor: gInteractor, router: DashboardRouterOutput(view))
        let presenter = DashboardPresenter(movieEntities: MovieEntities(), genreEntities: GenreEntities(), view: view, dependencies: dependencies, dependencies2: dependencies2)
        view.presenter = presenter
        view.mCollectionViewDataSource = MoviesCollectionViewDataSource(entities: presenter.movieEntities, presenter: presenter)
        view.gCollectionViewDataSource = GenresCollectionViewDataSource(entities: presenter.genreEntities, presenter: presenter)
        mInteractor.presenter = presenter
        gInteractor.presenter = presenter
        return view
    }

    func push(from: Viewable) {
        let view = self.view()
        from.push(view, animated: true)
    }

    func present(from: Viewable) {
        let nav = UINavigationController(rootViewController: view())
        from.present(nav, animated: true)
    }
}

final class DashboardRouterOutput: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }
    
    func showMovieDetail(movieDetailEntities: MovieDetailEntities) {
        print("Show movie detail")
        MovieDetailRouterInput().present(from: view, entryEntity: movieDetailEntities)
    }
    
    func showAllMovieByGenre(movieEntities: MovieEntities) {
        print("Show ALL Movie")
        MoviesRouterInput().present(from: view, entryEntity: movieEntities)
    }
}
