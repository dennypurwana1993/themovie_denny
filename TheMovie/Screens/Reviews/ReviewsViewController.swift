//
//  ReviewsViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 11/22/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class ReviewsViewController: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    internal var presenter: ReviewsViewOutputs?
    internal var reviewsCollectionViewDataSource: CollectionViewDataSource?
    @IBOutlet weak var reviewsCollectionView: UICollectionView! {
        didSet {
            reviewsCollectionView.delegate = self
            reviewsCollectionView.dataSource = self
            reviewsCollectionView.register(UINib.init(nibName: "ReviewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "reviewsCell")

        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

}

extension ReviewsViewController: ReviewsViewInputs {
    func reloadCollectionView(collectionViewDataSource: ReviewsCollectionViewDataSource) {
        self.reviewsCollectionViewDataSource = collectionViewDataSource
        DispatchQueue.main.async { [weak self] in
            self?.reviewsCollectionView.reloadData()
        }
    }
    
    func configure(entities: ReviewsEntities) {
        navigationItem.title = "Reviews"
        self.titleLbl.text = entities.apiState.movieTitle
    }
    
    
    func indicatorView(animate: Bool) {
       
    }
}

extension ReviewsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return reviewsCollectionViewDataSource?.numberOfItems ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
            return reviewsCollectionViewDataSource?.itemCell(collectionView: collectionView, indexPath: indexPath) ?? UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            return reviewsCollectionViewDataSource?.didSelect(collectionView: collectionView, indexPath: indexPath) ?? ()
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: (collectionView.bounds.width), height: 250 )
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}

extension ReviewsViewController: Viewable {}
